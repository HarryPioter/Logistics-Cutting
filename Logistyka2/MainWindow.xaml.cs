using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Python.Runtime;

namespace Logistyka2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ObservableCollection<AvailableItem> _available;
        private readonly ObservableCollection<RequestedItem> _requested;

        private readonly int firstSetup;
        private readonly int labelShift;

        public MainWindow()
        {
            InitializeComponent();
            firstSetup = 1;
            labelShift = 1 - firstSetup;

            _available = new ObservableCollection<AvailableItem>
            {
                new AvailableItem() {Size = 5.2},
            };
            AvailableGrid.ItemsSource = _available;

            _requested = new ObservableCollection<RequestedItem>
            {
                new RequestedItem() {Size = 0.7, Quantity = 2100},
                new RequestedItem() {Size = 2.5, Quantity = 1200},
            };

            RequestedGrid.ItemsSource = _requested;
//            InitPy();
        }

        private void InitPy()
        {
            PythonEngine.Initialize();

            using (Py.GIL())
            {
                dynamic sys = PythonEngine.ImportModule("sys");
                sys.path.append("../../");
            }

            CalcButton.IsEnabled = true;
        }


        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            InitPy();
        }

        private void CalcButton_Click(object sender, RoutedEventArgs e)
        {
            OutputGrid.HeadersVisibility = DataGridHeadersVisibility.Column;
            OutputGrid.Items.Clear();
            OutputGrid.Columns.Clear();
            using (Py.GIL())
            {
                dynamic chrom = PythonEngine.ImportModule("chrom");

                var available = _available.Select(a => a.Size).ToList();
                var required = _requested.Select(a => a.Size).ToList();
                var demand = _requested.Select(a => a.Quantity).ToList();
                var costPerUnit = parse(CostText.Text);

                PyObject dict = chrom.compute(available, required, demand, costPerUnit);
                var setupNum = ExtractSetupNumber(dict);
                var columnNum = setupNum + firstSetup;


                var rows = new string[_requested.Count][];
                for (var i = 0; i < _requested.Count; i++)
                {
                    rows[i] = new string[columnNum];
                    rows[i][0] = $"{formatDouble(required[i])} x {demand[i]}";
                }

                var setupSources = new double[columnNum];

                var x = firstSetup;
                foreach (PyObject setup in dict["setups"])
                {
                    var y = 0;
                    foreach (PyObject cell in setup)
                    {
                        setupSources[x] += parseRounded(cell)*required[y];
                        rows[y][x] = formatDecimal(cell);
                        ++y;
                    }
                    ++x;
                }
                foreach (var row in rows)
                {
                    OutputGrid.Items.Add(row);
                }

                var suits = OutputResults(dict, columnNum);
                var wastes = OutputWastes(dict, columnNum, costPerUnit, ref setupSources);

                var wasteSum = suits.Zip(wastes, (i, d) => new Tuple<int, double>(i, d))
                    .Select(tuple => tuple.Item1*tuple.Item2)
                    .Sum();

                var wasteSummary = $"{formatDouble(wasteSum)} m | {formatDouble(wasteSum * costPerUnit)} zł";


                var suitSummary = setupSources
                    .Zip(suits, (d, i) => new Tuple<double, int>(d, i))
                    .Where(tuple => tuple.Item2 != 0)
                    .GroupBy(
                        tuple => tuple.Item1,
                        tuple => tuple.Item2,
                        (d, ints) => $"{formatDouble(d)} x {ints.Sum()}"
                    )
                    .Aggregate((s1, s2) => $"{s1} | {s2}");

                StatusText.Text =
                    "Łączny odpad: " + wasteSummary + "\r\n" +
                    "Potrzebne rozmiary: " + suitSummary;

                OutputGrid.Columns.Add(new DataGridTextColumn
                {
                    Header = " ",
                    Binding = new Binding("[0]"),
                    MinWidth = 80
                });
                for (var i = firstSetup; i < columnNum; i++)
                {
                    OutputGrid.Columns.Add(new DataGridTextColumn
                    {
                        Header = $"{formatDouble(setupSources[i])}",
                        Binding = new Binding($"[{i}]"),
                        Width = new DataGridLength(1, DataGridLengthUnitType.Star),
                        MinWidth = 40
                    });
                }
            }
        }

        private static int ExtractSetupNumber(PyObject dict)
        {
            return int.Parse(dict["setups_count"].ToString());
        }

        private int[] OutputResults(PyObject dict, int columnNum)
        {
            var sum = 0;
            var results = new string[columnNum];
            var suits = new int[columnNum];

            results[0] = "Komplety";


            var v = firstSetup;
            foreach (PyObject cell in dict["result"])
            {
                sum += (int) parseRounded(cell);
                results[v] = formatDecimal(cell);
                suits[v] = (int) parseRounded(cell);
                ++v;
            }

            OutputGrid.Items.Add(results);

            return suits;
        }

        private double[] OutputWastes(PyObject dict, int columnNum, double costPerUnit, ref double[] setupSources)
        {
//            var wasteSum = 0.0;
//            var costSum = 0.0;

            var wastes2 = new double[columnNum];
            var wastes = new string[columnNum];
            var costs = new string[columnNum];

            wastes[0] = "Odpad (m)";
            costs[0] = "Odpad (zł)";

            var u = firstSetup;
            foreach (PyObject cell in dict["setups_waste"])
            {
                var waste = parse(cell);
                var cost = waste*costPerUnit;
//                wasteSum += waste;
//                costSum += cost;
                setupSources[u] += waste;
                wastes2[u] = waste;
                wastes[u] = formatDouble(waste);
                costs[u] = formatDouble(cost);
                ++u;
            }

            OutputGrid.Items.Add(wastes);
            OutputGrid.Items.Add(costs);

            return wastes2;
        }

        private static string formatDecimal(PyObject d)
        {
            return $"{parseRounded(d):F0}";
        }

        private static string formatDouble(PyObject d)
        {
            return $"{parse(d):F2}";
        }

        private static string formatDouble(double d)
        {
            return $"{d:F2}";
        }

        private static double parse(string str)
        {
            return double.Parse(str.Replace(".", ","));
        }

        private static double parse(PyObject d)
        {
            return double.Parse(d.ToString().Replace(".", ","));
        }

        private static double parseRounded(PyObject d)
        {
            return Math.Round(parse(d));
        }
    }
}
