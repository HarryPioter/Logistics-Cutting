import numpy as np
from itertools import product
from scipy.optimize import minimize

def compute(available, required, demands, cost):
    return get_minimalization_result(list(available), list(required), list(demands), cost)

def get_minimalization_result(available, required, demands, cost=0):
    setups_and_waste = get_flatten_division_setups_and_wastes(available, required)
    # print(setups_and_waste)
    constraints = generate_constraint_lambdas(setups_and_waste['setups'], demands)
    target_function = generate_target_function(setups_and_waste['waste'])
    variables_count = len(setups_and_waste['waste'])
    bounds = generate_bounds(variables_count)
    variables = [1.0] * variables_count
    res = minimize(target_function, variables, method='SLSQP', bounds=bounds, constraints=constraints)

    if res.success:
        wasted_len = np.dot(res.x, setups_and_waste['waste'])
        return {'waste': wasted_len,
                'cost': wasted_len * cost,
                'required': required,
                'setups': setups_and_waste['setups'],
                'setups_waste': setups_and_waste['waste'],
                'result': res.x.flatten().tolist(),
                'setups_count': len(setups_and_waste['setups']),
                'demands': demands
                }
    else:
        return {}


def print_result(minimalization):
    if len(minimalization) > 0:
        print('{0}  - required parts'.format(minimalization['required']))
        for setup, count in zip(minimalization['setups'], minimalization['result']):
            print('{0} x {1}'.format(setup, count))
        transposition = np.transpose(minimalization['setups'])
        print('\nWasted : {0} => cost = {1}\n'.format(minimalization['waste'],
                                                      minimalization['waste'] * minimalization['cost']))
        for demand, length, vector in zip(minimalization['demands'], minimalization['required'], transposition):
            print('{0} expected: {1}, got: {2}'.format(length, demand, np.dot(vector, minimalization['result'])))
    else:
        print('Unsuccessful optimization')


def get_flatten_division_setups_and_wastes(available, required):
    possible_setups = [get_all_valid_permutations(required, length) for length in available]
    wastes = [calculate_waste(setups, required, length) for setups, length in zip(possible_setups, available)]
    # print(possible_setups)
    # print(wastes)
    flat_setups = [item for sublist in possible_setups for item in sublist]
    flat_wastes = [item for sublist in wastes for item in sublist]
    final_setups = []
    final_wastes = []
    for setup, waste in zip(flat_setups, flat_wastes):
        if setup not in final_setups:
            final_setups.append(setup)
            final_wastes.append(waste)
    # print(flat_setups)
    # print(flat_wastes)
    # print(final_setups)
    # print(final_wastes)
    return {'setups': final_setups, 'waste': final_wastes}


def get_all_valid_permutations(partial_lengths, whole_length):
    maxes = [int(whole_length / length) for length in partial_lengths]
    ranges = [list(range(max_amount + 1)) for max_amount in maxes]
    valid_combinations = []
    length_of_min_part = min(partial_lengths)
    possible_setups = product(*ranges)
    for setup in possible_setups:
        waste = whole_length - np.dot(setup, partial_lengths)
        if (waste < length_of_min_part) and (waste >= 0):
            valid_combinations.append(setup)
    # print(valid_combinations)
    return valid_combinations


def calculate_waste(apportionments, parts, total_length):
    waste = []
    for portions in apportionments:
        parts_length_sum = 0.0
        for amount, part_length in zip(portions, parts):
            parts_length_sum += amount * part_length
        waste.append(total_length - parts_length_sum)
    return waste


def calculate_single_waste(setup, lengths, total_length):
    return total_length - np.dot(setup, lengths)


def generate_bounds(variables_count):
    bounds = []
    for i in range(variables_count):
        bounds.append((0, None))
    return bounds


def generate_constraint_lambdas(apportionments, demands):
    transposition = np.transpose(apportionments)
    # print(transposition)
    lambdas = [lambda variables, vec=vector, dmd=demand: np.dot(variables, vec) - dmd for vector, demand in
               zip(transposition, demands)]
    return [{'type': 'eq', 'fun': func} for func in lambdas]


def generate_target_function(waste):
    return lambda variables: np.dot(variables, waste)
