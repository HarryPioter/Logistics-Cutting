import chrom as cr

# 1 zadanie
available = [5.2]
required = [0.7, 2.5]
demand = [2100.0, 1200.0]

# 2 zadanie
# available = [2.1, 4.2]
# required = [0.5, 1.4]
# demand = [12000.0, 18000.0]

# 3 zadanie
# available = [30.0]
# required = [11.0, 8.0, 5.0]
# demand = [12000.0, 24000.0, 27000.0]

ans = cr.get_minimalization_result(available, required, demand, 0)  # cost is 0 by default
print(ans)
cr.print_result(ans)

# wynik minimalizacji:
# {
#  'required': lista wymaganych długości - redundant
#  'demands': wymagania ilościowe względem każdej wymaganej długości - redundant
#  'waste': sumaryczna długość odpadu - float
#  'cost': koszt odpadu - float
#  'setups_count': ilosć rozkrojów, int
#  'setups': lista rozkrojów, rozkrój to tupla, lista tupli
#  'setups_waste': lista odpadu dla każdego rozkroju, lista floatów
#  'result': wektor rozwiązania, lista floatów
#  }
