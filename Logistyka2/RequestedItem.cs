namespace Logistyka2
{
    public class RequestedItem
    {
        public double Size { get; set; }
        public int Quantity { get; set; }
    }
}
