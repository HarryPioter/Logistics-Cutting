# Logistics-Cutting
University collab project. Tucked together by [Tomasz](https://github.com/tomblachut) & [Piotr](https://github.com/PiotrChromniak).

## Dependencies
This project is an example of C# & Python 3.6 interop, and `pythonnet` is required to run it. We recommend `WinPython` distribution, which includes `pythonnet`. 

## Screenshots
![Initial screen](https://raw.githubusercontent.com/tomblachut/Logistics-Cutting/master/doc/pre.png)
![After optimization](https://raw.githubusercontent.com/tomblachut/Logistics-Cutting/master/doc/post.png)
